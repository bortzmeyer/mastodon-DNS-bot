# DNS Fediverse bot

This bot listens to Fediverse (Mastodon and others) mentions sent to
it, performs a DNS resolution on the domain name indicated and returns
the result. After the domain name, you may optionally add a query type
such as MX or NS (default query type is A).

## Usage 

An instance of the bot runs at `@DNSresolver@botsin.space`. Just write to
it a domain name.

Here is how it looks like in the default Web client. First, a query
without an explicit qtype:

![](query-no-qtype.png)

Then with the reply:

![](response-no-qtype.png)

A query with an explicit qtype, the little-known LOC (location):

![](query-qtype.png)

Then with the reply:

![](response-qtype.png)

Here are a few other examples as text:

* `www.afnic.fr` : you will receive the A record (IPv4 address) of
  this server
* `北京大学.中国` : yes, IDN work
* `مركزأسماءالنطاقات.الجزائر` : right-to-left, too
* `réussir-en.fr AAAA` : here, we indicate a non-default query type,
  AAAA (IPv6 address)
* `cloudflare.com DNSKEY` : most DNSSEC keys will be too large for Mastodon if they use RSA, but ECDSA is fine. Note that other fediverse implementation such as Pleroma has a higher character limit.
* `144.76.206.42` : yes, IP addresses are handled sensibly
* `2001:67c:2218:1b::51:99`

If you want to use unknown or little-known DNS resource record types,
prefix their number with `TYPE`, for instance `bortzmeyer.fr TYPE262`
for the recent (june 2024) WALLET type.

If you use the excellent
[madonctl tool](https://github.com/McKael/madonctl), you can send
toots on the command line. Here are two examples:

```
madonctl toot --visibility direct "@DNSresolver@botsin.space framagit.org"

madonctl toot --visibility direct "@DNSresolver@botsin.space mastodon.mg NS"
```

## More readings

You can see [my article on the implementation of this bot](http://www.bortzmeyer.org/fediverse-bot.html).

## Installation

TODO. In the mean time, see the list of dependencies in the source code

To create the secrets needed by the bot, see `register.py`.

## About the Fediverse and Mastodon

The Fediverse is the generic name of the decentralized social network made of all the instances running compatible protocols, such as ActivityPub. [Mastodon](https://joinmastodon.org/) is one of the implementations, built on free software. Users can send short messages, called "toots".

## Reference site

[On FramaGit](https://framagit.org/bortzmeyer/mastodon-DNS-bot)

## Author

Stéphane Bortzmeyer <bortzmeyer@afnic.fr>
