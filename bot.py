#!/usr/bin/env python3

# A Fediverse (decentralized social network, for instance using Mastodon) bot
# to answer to DNS requests.
# Stéphane Bortzmeyer bortzmeyer@afnic.fr
# Official repository at <https://framagit.org/bortzmeyer/mastodon-DNS-bot>

# Defaults that you will probably change
NAMESERVER = "::1" # Set it to None to use the default resolver(s)
MYNAME = "DNSresolver"
MYDOMAIN = "botsin.space"
HOME="/home/dnsresolver/" # Don't forget the slash at the end
BLACKLIST = ["badguy@mastodon.example", "evilinstance.example"] # You can add accounts or entire instances.
# TODO: canonicalize the case?

# Defaults that you may change
TIMEOUT = 5 # seconds
LOGGER = MYNAME
MAXLOG = 100 # Maximum size of answers to log
SLEEP_DELAY = 0.75 # Sleep time between DNS requests
RESTART_DELAY = 0.5 # To avoid restart crazy loops
TIMER_DELAY = 9 
EDNS_SIZE = 4096 # Set it to None to disable EDNS
DEBUG=False

# Defaults that are better left alone
MASTODON_MAX = 500 # This is Mastodon default size. Other fediverse programs
                   # have a different limit (5 000 by default for Pleroma) but
                   # we have to be conservative, to serve everybody.
MAXIMUM_HEARTBEAT_DELAY = 50 # Heartbeats are sent every 15 seconds

# Documentation in <https://mastodonpy.readthedocs.io/en/latest/> Note it
# should work with all the programs using the Mastodon API, such as Pleroma,
# but it was not tested yet.
from mastodon import Mastodon, StreamListener

# http://lxml.de/
from lxml import html

# http://www.dnspython.org/
# Documentation in <http://www.dnspython.org/docs/1.15.0/>
from dns import resolver, rdatatype, name, exception, reversename, flags

import re
import logging
import sys
import time
import string
import socket
import os
import fcntl
import multiprocessing
import tempfile

def is_ip_address(str):
    try:
        addr = socket.inet_pton(socket.AF_INET6, str)
    except socket.error: # not a valid IPv6 address
        try:
            addr = socket.inet_pton(socket.AF_INET, str)
        except socket.error: # not a valid IPv4 address either
            return False
    return True

class myListener(StreamListener):

    def __init__(self, masto, log, hb_filename, pid_filename):
        self.resolver = resolver.Resolver()
        self.resolver.lifetime = TIMEOUT
        if NAMESERVER is not None:
            self.resolver.nameservers = [NAMESERVER]
        if EDNS_SIZE is not None:
            self.resolver.use_edns(0, flags.DO, EDNS_SIZE)
        self.hb_filename = hb_filename
        self.pid_filename = pid_filename
        self.blacklist = BLACKLIST
        self.blacklist.append("%s@%s" % (MYNAME, MYDOMAIN)) # To avoid loops
        self.masto = masto
        self.log = log
        self.pid_file = open(self.pid_filename, 'w')
        fcntl.lockf(self.pid_file, fcntl.LOCK_EX)
        print("%s" % os.getpid(), file=self.pid_file)
        fcntl.lockf(self.pid_file, fcntl.LOCK_UN)
        self.pid_file.close()
        self.heartbeat_file = open(self.hb_filename, 'w')
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_EX)
        print(time.time(), file=self.heartbeat_file)
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_UN)
        self.heartbeat_file.close()
        super()

    def handle_heartbeat(self):
        self.log.debug("HEARTBEAT") # Sent every 15 seconds by Mastodon        
        self.heartbeat_file = open(self.hb_filename, 'w')
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_EX)
        print(time.time(), file=self.heartbeat_file)
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_UN)
        self.heartbeat_file.close()
                                                                    
    def on_notification(self, notification):
        # The bot is strictly sequential, handling one notification
        # after the other. Because a DNS request can take a lot of
        # time to process, this is not satisfying. In theory, it would
        # be better to have a parallel bot, using the threading module
        # <https://docs.python.org/3/library/threading.html>. But note
        # it would increase the risk of denial-of-service, in case
        # there are many requests. May be a better rate-limiting would
        # help.
        try:
            qname = None
            qtype = None
            sender = None
            visibility = None
            spoiler_text = None
            if notification['type'] == 'mention':
                id = notification['status']['id']
                sender = notification['account']['acct']
                if sender in BLACKLIST:
                    self.log.info("Service refused to %s" % sender)
                    return
                match = re.match(r"^.*@(.*)$", sender)
                if match:
                    sender_domain = match.group(1)
                    if sender_domain in BLACKLIST:
                        self.log.info("Service refused to %s" % sender)
                        return
                else:
                    # Probably local instance, without a domain name. Note that we cannot blacklist local users.
                    if sender == MYNAME:
                        self.log.info("Loop detected, sender is myself")
                        return
                # TODO Rate-limit per-user?
                visibility = notification['status']['visibility']
                spoiler_text = notification['status']['spoiler_text']
                # Mastodon API returns the content of the toot in
                # HTML, just to make our lifes miserable
                doc = html.document_fromstring(notification['status']['content'])
                # Preserve end-of-lines
                # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
                for br in doc.xpath("*//br"):
                            br.tail = "\n" + br.tail if br.tail else "\n"
                for p in doc.xpath("*//p"):
                            p.tail = "\n" + p.tail if p.tail else "\n"
                body = doc.text_content()
                # Handling Misskey users
                if MYNAME + "@" + MYDOMAIN in body:
                    match = re.match(r"^@%s\s+([\w\-\.:]+)(\s+([0-9a-zA-Z]+))?(\s*(.*))?$" % (MYNAME + "@" + MYDOMAIN), body)
                else:
                    match = re.match(r"^@%s\s+([\w\-\.:]+)(\s+([0-9a-zA-Z]+))?(\s*(.*))?$" % MYNAME, body) 
                if match:
                    sname = match.group(1)
                    if not is_ip_address(sname):
                        qname = sname
                    else:
                        qname = reversename.from_address(sname)
                    if match.group(3) is not None and match.group(3) != '':
                        qtype = match.group(3).upper()
                    else:
                        if not is_ip_address(sname):
                            qtype = 'A'
                        else:
                            qtype = 'PTR'
                    expected = True
                    if match.group(5) is not None and match.group(5) != '':
                        answers = "Spurious text at the end: syntax is just domainname plus optional query type"
                    else:
                        try:
                            msg = self.resolver.resolve(qname, qtype, search=False)
                            answers = ""
                            for data in msg:
                                if qtype == 'TXT': # TXT record values are just a list of bytes.
                                    # But we are trying to display them nicely, by first testing
                                    # if they are in UTF-8.
                                    try:
                                        for s in data.strings:
                                            answers = answers + s.decode("UTF-8") + "\n"
                                    except UnicodeDecodeError: # OK, probably not UTF-8, let's just display them
                                        answers = answers + str(data) + "\n"
                                else:
                                    answers = answers + str(data) + "\n"
                            if (msg.response.flags & flags.AD):
                                answers = answers + "\n" + "✅ [Authenticated by DNSSEC]\n"
                        except resolver.NoAnswer:
                            answers = "NODATA (name exists but no data of type %s)" % qtype
                        except resolver.NXDOMAIN:
                            answers = "NXDOMAIN (name does not exist)"
                        except resolver.NoMetaqueries:
                            answers = "ERROR Hey, what did you expect? ANY queries are forbidden"
                        except rdatatype.UnknownRdatatype:
                            answers = "ERROR Unknown query type %s" % qtype
                        except resolver.NoNameservers as error:
                            answers = "ERROR No proper answer (may be a network or DNSSEC issue with %s): %s" % (qname, error) # It would be better to analyze the result,
                            # the generic error message may be too long / detailed
                        except name.LabelTooLong:
                            answers = "ERROR One of the labels of this name is too long"
                        except exception.Timeout: 
                            answers = "ERROR Timeout (may be a network problem with the authoritative name servers)" 
                        except UnicodeError as error:
                            expected = False
                            answers = "ERROR Something wrong with your Unicode string: %s" % error
                        except Exception as error: # TODO may be too revealing on internal
                            # implementation (but this is free software, after all)
                            expected = False
                            answers = "ERROR General failure: %s" % error
                    canswers = answers.replace('@', '(at)') # Rude but
                    # this is to address the case of Mastodon
                    # addresses in a TXT record. May be indicating the
                    # status as a dict with an explicit list of
                    # recipients solves the problem, but I didn't
                    # test.
                    text = "@%s %s" % (sender, canswers)
                    if len(text) >= MASTODON_MAX:
                        text = "@%s Sorry, answer is %d characters, too large for Mastodon" % (sender, len (text))
                    self.masto.status_post(text, in_reply_to_id = id, 
                                           visibility = visibility, spoiler_text = spoiler_text)
                    lanswers = canswers.replace('\n', ' ')[0:MAXLOG]
                    if expected:
                        self.log.info("%s %s/%s -> %s - %s" % (sender, qname, qtype, lanswers, visibility)) 
                    else:
                        self.log.error("%s %s/%s -> %s - %s" % (sender, qname, qtype, lanswers, visibility))
                else:
                    pass # Produces an error message, at least? Be
                         # careful not to answer if we are just
                         # included in a conversation. Or log it
                         # (dangerous if toot-bombing)?
                time.sleep(SLEEP_DELAY) # Crude rate-limiting 
        except KeyError as error:
            self.log.error("Malformed notification, missing %s" % error)
        except Exception as error: 
            self.log.error("%s %s/%s -> %s" % (sender, qname, qtype, error))

def driver(log, heartbeat_filename, pid_filename):
    try:
        mastodon = Mastodon(
            client_id = "%s_clientcred.secret" % MYNAME,
            access_token = "%s_usercred.secret" % MYNAME,
            api_base_url = "https://%s/" % MYDOMAIN,
            debug_requests = DEBUG)
        listener = myListener(mastodon, log, heartbeat_filename, pid_filename)
        log.info("Driver/listener starts, PID %s" % os.getpid())
        if not mastodon.stream_healthy():
            log_critical("Streaming server is not healthy")
            return False
        mastodon.stream_user(listener)
    except Exception as error:
        # From time to time, we receive exceptions "('Bad event
        # type', 'announcement.reaction')". According to
        # <https://github.com/halcy/Mastodon.py/issues/218>, it is an
        # issue in Mastodon.py, waiting to be fixed.
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log.critical("Unexpected error %s in the driver \"%s\"" % (exc_type.__name__, error))

log = logging.getLogger("LOGGER")
channel = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
channel.setFormatter(formatter)
log.addHandler(channel)
log.setLevel(logging.INFO)
while True:
    try:
        tmp_hb_filename = tempfile.mkstemp(prefix=HOME + "dns-bot-", suffix='.heartbeat')
        tmp_pid_filename = tempfile.mkstemp(prefix=HOME + "dns-bot-", suffix='.pid')
        log.debug("Creating a new process")
        proc = multiprocessing.Process(target=driver, args=(log, tmp_hb_filename[1],tmp_pid_filename[1]))
        proc.start()
        while True:
            time.sleep(TIMER_DELAY)
            if proc.is_alive():
                h = open(tmp_hb_filename[1], 'r')
                fcntl.lockf(h, fcntl.LOCK_SH)
                data = h.read(128)
                fcntl.lockf(h, fcntl.LOCK_UN)
                h.close()
                try:
                    last_heartbeat = float(data)
                except ValueError: # TODO there is a bug somewhere
                                   # since we sometimes get empty
                                   # heartbeat files. That's strange
                                   # since it is supposed to be filled
                                   # in when the listener starts, even
                                   # before the first heartbeat is
                                   # received. May be establishing the
                                   # session failed, triggering an
                                   # exception, and leaving the file
                                   # unfilled?
                    log.error("Invalid value in heartbeat file %s: \"%s\"" % (tmp_hb_filename[1], data))
                    last_heartbeat = 0
                if time.time() - last_heartbeat > MAXIMUM_HEARTBEAT_DELAY:
                    log.error("No more heartbeats, kill %s" % proc.pid)
                    proc.terminate()
                    proc.join()
                    log.debug("Done, it exited with code %s" % proc.exitcode)
                    try:
                        os.remove(tmp_hb_filename[1])
                    except:
                        pass
                    try:
                        os.remove(tmp_pid_filename[1])
                    except:
                        pass
                    break
            else:
                log.error("Bot died, we restart it")
                time.sleep(RESTART_DELAY)
                try:
                    os.remove(tmp_hb_filename[1])
                except:
                    pass
                try:
                    os.remove(tmp_pid_filename[1])
                except:
                    pass
                break
    except Exception as error:
        log.critical("Unexpected error in the timer: \"%s\"" % error)
        try: # Try to clean
            proc.terminate()
            proc.join()
            try:
                os.remove(tmp_hb_filename[1])
            except:
                pass
            try:
                os.remove(tmp_pid_filename[1])
            except:
                pass
        except:
            pass
        time.sleep(RESTART_DELAY)
                                                                                                                                                                                       
